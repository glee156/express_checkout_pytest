import pytest
import json
import chromedriver_binary
import os
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption("--env", action="store", default="staging", help="Specify test enviroment")

@pytest.fixture
def globals(request):
    settings = json.load(open('settings.json'))
    env_global = settings[request.config.getoption("--env")]
    return env_global

@pytest.fixture
def driver():
    chrome_options = webdriver.ChromeOptions()
    profile = {"download.default_directory": os.path.expanduser('~/Downloads'),
               "download.prompt_for_download": False,
               "download.directory_upgrade": True,
               "plugins.plugins_disabled": ["Chrome PDF Viewer"],
               'credentials_enable_service': False,
               'password_manager_enabled': False}

    chrome_options.add_experimental_option("prefs", profile)
    chrome_options.add_argument("--disable-extensions")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.set_window_size(1800, 1400)
    driver.implicitly_wait(5)
    yield driver
    driver.quit()

@pytest.fixture
def mobile_driver():
    chrome_options = webdriver.ChromeOptions()
    profile = {"download.default_directory": os.path.expanduser('~/Downloads'),
               "download.prompt_for_download": False,
               "download.directory_upgrade": True,
               "plugins.plugins_disabled": ["Chrome PDF Viewer"],
               'credentials_enable_service': False,
               'password_manager_enabled': False
               }

    chrome_options.add_experimental_option('mobileEmulation', {'deviceName': 'iPhone 8'})
    chrome_options.add_experimental_option("prefs", profile)
    chrome_options.add_argument("--disable-extensions")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.set_window_size(1800, 1400)
    driver.implicitly_wait(5)
    yield driver
    driver.quit()