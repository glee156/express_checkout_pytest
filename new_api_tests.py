import sys
import time
import logging
import datetime
import calendar
from tests import test_case
from tests import test_suite
import tests.web.WEB.API.common.constants as cc
import tests.web.WEB.API.common.status_codes as sc
from tests.web.WEB.API.endpoints.id_requester import id_requester
from selenium.webdriver.remote.remote_connection import LOGGER
from tests.web.TESTS.api_tests.license.products_post_get_put import steps
from tests.web.TESTS.api_tests.license.products_post_get_put.license_api_tests_post_and_put_subscription_licenses import delete_all_products_for_a_user


def products_test_scenarios():
    LOGGER.setLevel(logging.WARNING)  # Only show critical warning logs
    # products_product_id_put_missing_serato_checkout_order_parameter()
    request_to_test_gaclientid_endpoint_success()
    # request_to_identity_api_with_valid_credentials_is_successful()
    # request_to_identity_api_with_ga_client_id_and_expired_token_is_unsuccessful()


def products_product_id_put_missing_serato_checkout_order_parameter():
    # SETUP
    delete_all_products_for_a_user(cc.LICENSE_API_CHECKOUT_ORDER_ID)

    # GIVEN
    token = cc.BASIC_AUTH_TOKEN
    checkout_order_id = cc.LICENSE_API_CHECKOUT_ORDER_ID
    checkout_order_item_id = cc.LICENSE_API_CHECKOUT_ORDER_ITEM_ID
    subscription_product_type_ids = [cc.PRODUCT_TYPE_SERATO_DJ_SUB, cc.PRODUCT_TYPE_EXP_PACK_SUB]

    # I post a list of permanent licenses to a user
    steps.request_to_license_api_post_subscription_license_to_user(token=token,
                                                                   subscription_product_type_ids=subscription_product_type_ids,
                                                                   user_id=cc.LICENSE_API_USER_ID,
                                                                   valid_to=cc.LICENSE_API_VALID_DATE,
                                                                   checkout_order_id=checkout_order_id,
                                                                   checkout_order_item_id=checkout_order_item_id,
                                                                   expected_response=200)

    # I return a list of the product ids assigned to the user
    product_ids = steps.request_to_license_api_get_magento_order_id_returns_the_product_id(token=token,
                                                                                           checkout_order_id=checkout_order_id,
                                                                                           generic_product_type_ids=subscription_product_type_ids,
                                                                                           expected_response=200)

    # WHEN
    # I update product info using the product ids
    # THEN
    # I should get a 400 error
    steps.request_to_license_api_to_update_subscription_license_product_info(token=token,
                                                                             product_ids=product_ids,
                                                                             checkout_order_id=checkout_order_id,
                                                                             expected_response=400)


def request_to_test_gaclientid_endpoint_success():
    test_case.log_title(sys._getframe(0).f_code.co_name)

    # SETUP
    # I create a new account with a POST request to 'users'
    iso_timestamp = datetime.datetime.now().isoformat()
    epoch_timestamp = calendar.timegm(time.gmtime())
    unique_email = cc.TEST_USERS['VALID_TEST_USER']['email'].replace('@', '+' + str(epoch_timestamp) + '@')  # Add a timestamp to the email to make a unique user
    request_new_user_response_object = id_requester('POST', 'users', email=unique_email, password=cc.TEST_USERS['VALID_TEST_USER']['password'], timestamp=iso_timestamp)

    # GIVEN
    # I have the following User ID and GA Client ID
    user_id = request_new_user_response_object.json()['id']
    ga_client_id = cc.TEST_USERS['VALID_TEST_USER']['ga_client_id']

    # WHEN
    # I make a POST request to 'users/{user_id}/gaclientid'
    endpoint = "users/{user_id}/gaclientid".format(user_id=user_id)
    request_ga_client_id_response_object = id_requester('POST', endpoint, ga_client_id=ga_client_id)

    # THEN
    # I have the GA Client ID is associated with me
    sc._status_code_is_(200, request_ga_client_id_response_object)
    request_existing_user_response_object = id_requester('GET', 'users', params={'ga_client_id': ga_client_id})
    assert request_existing_user_response_object.json()['items'][0]['id'] == user_id


'''def request_to_identity_api_with_ga_client_id_and_expired_token_is_unsuccessful():
    test_case.log_title(sys._getframe(0).f_code.co_name)

    # GIVEN the user has the following User ID and GA Client ID
    user_id = cc.TEST_USERS['VALID_TEST_USER']['user_id']
    ga_client_id = cc.TEST_USERS['VALID_TEST_USER']['ga_client_id']

    # WHEN the user makes a POST request to 'users/{user_id}/gaclientid' with an expired token
    endpoint = "users/{user_id}/gaclientid".format(user_id=user_id)
    request_ga_client_id_response_object = id_requester('POST', endpoint, ga_client_id=ga_client_id, token=cc.EXPIRED_ACCESS_TOKEN)

    # THEN there should be a 400 error? with message about expired token. Nope api doesn't use tokens for authentication
    # It uses HTTP Basic Access Authentication header instead which encodes app id and app secret
    sc._status_code_is_(400, request_ga_client_id_response_object)'''


# =======================================================================
# ========================== FRAMEWORK  TEST  ===========================
# =======================================================================

def main(*args, **kwargs):
    with test_suite.Suite():
        products_test_scenarios()

if __name__ == '__main__':
    main()