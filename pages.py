from tests.web.WEB.Seratodotcom.Steps import express_checkout
from tests.web.WEB.Seratodotcom.Pages.express_checkout import your_cart
from tests.web.WEB.Seratodotcom.Pages import common_home
from tests.web.WEB.Seratodotcom.Pages.express_checkout import log_in_page
from tests.web.WEB.Seratodotcom.Pages.express_checkout import sign_up_page
from tests.web.UTILITIES.Email_functions import common_pop3
from utilities import misc
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from tests.web.WEB.Seratodotcom.Pages.express_checkout import payment_page
from tests.web.WEB.API.endpoints import checkoutlib_requesters
import datetime