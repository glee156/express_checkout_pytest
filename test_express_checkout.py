import pages as pages

def test_cancel_order_with_referrer(driver, globals):
    # When I go to cart with referrer

    driver.get(globals['base_url'] + '/checkout/cart/item/add/58?referrer=dj/pricing')
    # Then the back button should be linked to the referrer page

    assert pages.your_cart.back_button(driver).get_attribute('href') == globals['base_url'] + '/dj/pricing'


def test_cancel_order_without_referrer(driver, globals):
    # When I go to cart without referrer

    driver.get(globals['base_url'] + '/checkout/cart/item/add/58')
    # Then the back button should be linked to home page

    assert pages.your_cart.back_button(driver).get_attribute('href') == globals['base_url'] + '/'


def test_invoice_details_change_country(driver, globals):
    timestamp = pages.misc.get_datetime_now_string()
    username = globals['test_accounts'][0]['valid_email']
    email = username.split("@")[0] + "+" + timestamp + "@" + username.split("@")[1]
    password = globals['test_accounts'][0]['valid_password']
    test_env = globals['base_url']

    # Given I am a new NZ user
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.log_in_page.create_account_button(driver).click()
    pages.express_checkout.signup(driver, email, password, country="New Zealand")

    # When I change my country from New Zealand to Germany
    pages.payment_page.enter_invoice_details(driver, country='Germany')

    # Then the tax rate should be changed
    assert pages.payment_page.tax_percentage_text(driver) == '19%'


def test_confirm_user_page_and_change_user(driver, globals):
    username = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    test_env = globals['base_url']
    changed_username = globals['test_accounts'][2]['changed_email']

    # GIVEN
    # I log into serato.com and go to pricing page
    pages.common_home.log_in_to_serato_dot_com(driver, test_env, username, password)
    assert pages.common_home.the_user_is_logged_in(driver, username)
    pages.express_checkout.go_to_pricing_page(driver, test_env)

    # WHEN
    # I add dj suite purchase to cart and checkout
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # THEN
    # I see the Confirm User page with the email I just logged in with
    pages.express_checkout.i_should_see_my_user_on_the_confirm_user_page_as(driver, username)

    # WHEN
    # I click on the link to change user and log in
    pages.express_checkout.i_change_user_after_logged_in(driver)
    pages.express_checkout.log_in_with_existing_user(driver, changed_username, password)

    # THEN
    # I should see the payment details form
    pages.express_checkout.i_should_see_the_payment_details_form(driver)


def test_log_in_as_existing_user(driver, globals):
    username = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    test_env = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # WHEN
    # I log in with an existing user account
    pages.express_checkout.log_in_with_existing_user(driver, username, password)

    # THEN
    # I should see the Payment page
    pages.express_checkout.i_should_see_the_payment_details_form(driver)


def test_log_in_using_invalid_email(driver, globals):
    test_env = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # WHEN
    # I log in with a non-existent user account
    pages.express_checkout.log_in_with_nonexistent_user(driver)

    # THEN
    # TODO: Wait for the invalid email messaging to be implemented on the page
    assert pages.log_in_page.error_message_text(driver).text == 'Invalid login credentials'


def test_log_in_as_user_without_braintree_vault_id(driver, globals):
    '''
       During testing it was found that being logged with a user that has no
       Customer ID in the Braintree Vault means that they cannot access the
       Express Checkout cart. "test.user+001@serato.com" has been reserved as
       a user without a Customer ID in the test-7 stack.
       :return:
    '''
    test_env = globals['base_url']
    username_no_braintree_id = globals['test_accounts'][2]['changed_email']
    password = globals['test_accounts'][0]['valid_password']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.common_home.log_in_to_serato_dot_com(driver, test_env, username_no_braintree_id, password)
    pages.common_home.the_user_is_logged_in(driver, username_no_braintree_id)
    pages.express_checkout.go_to_pricing_page(driver, test_env)

    # WHEN
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # THEN
    # I see the Confirm User page with the email I just logged in with
    pages.express_checkout.i_should_see_my_user_on_the_confirm_user_page_as(driver, username_no_braintree_id)

    # WHEN
    pages.express_checkout.continue_to_payment_page(driver)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)


def test_request_to_login_with_valid_credentials_is_successful():
    result = pages.checkoutlib_requesters.login_requester(test_environment=1, email='willem.yang+testing@serato.com', password='111')
    assert result['emailAddress'] == 'willem.yang+testing@serato.com'


def test_request_to_login_with_invalid_credentials_is_unsuccessful():
    result = pages.checkoutlib_requesters.login_requester(test_environment=1, email='willem.yang+testing@serato.com', password='1')
    assert result['error'] == 'Invalid login credentials'


def test_request_to_logout_is_successful():
    result = pages.checkoutlib_requesters.logout_requester(test_environment=1)
    assert result['message'] == 'OK'


def test_express_checkout_purchase_permanent_using_credit_card(driver, globals):
    '''
        Users could subscribe to more than one subscription on the launch of
        eCommerce Express Checkout.
        Users cannot subscribe to more than one subscription as of WE-242.
        :param billing_country:
        :param driver:
        :return:
    '''

    test_env = globals['base_url']
    country = globals['country']
    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    tax_rate = globals['tax']['tax_rate']
    tax_percentage = globals['tax']['tax_percentage']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.common_home.log_in_to_serato_dot_com(driver, test_env, email, password)
    pages.express_checkout.go_to_pricing_page(driver, test_env)

    # WHEN
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.change_billing_country(driver, country)
    pages.WebDriverWait(driver, 5).until(
        pages.EC.invisibility_of_element_located((pages.By.XPATH, "//div[@class='spinner']")))
    pages.express_checkout.i_should_see_tax_calculated_on_my_order(driver, tax_rate, tax_percentage)
    pages.express_checkout.continue_order_checkout(driver)

    # THEN
    # I see the Confirm User page with the email I just logged in with
    pages.express_checkout.i_should_see_my_user_on_the_confirm_user_page_as(driver, email)

    # WHEN
    pages.express_checkout.continue_to_payment_page(driver)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    pages.express_checkout.select_the_first_saved_payment_method(driver)
    # pages.express_checkout.enter_new_user_invoice_details(driver)
    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    pages.express_checkout.the_purchase_is_successful(driver, "Serato DJ Suite", email)


def test_express_checkout_new_user_purchase_subscription_new_credit_card_payment(driver, globals):
    '''
        Subscription saved payment methods can't be deleted by design as there is a
        recurring subscription payment attached to them.
        :return:
        '''
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, base_url)

    # WHEN
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method
    pages.express_checkout.add_credit_card_payment_method(driver, cvv=111)

    pages.express_checkout.enter_new_user_invoice_details(driver)

    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    pages.express_checkout.the_subscription_is_successful(driver, "Serato DJ Suite Subscription", email)


'''tests with [add_credit_card_payment_method > fill_in_card] fails with newest chromedriver so added wait_in_seconds(1, "bug in latest selenium")'''
def test_express_checkout_new_user_purchase_permanent_new_credit_card_payment_delete_payment_method(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, base_url)

    # WHEN
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method
    pages.express_checkout.add_credit_card_payment_method(driver, cvv=111)

    pages.express_checkout.enter_new_user_invoice_details(driver)

    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    pages.express_checkout.the_purchase_is_successful(driver, "Serato DJ Suite", email)

    # GIVEN
    # Delete a payment method
    pages.express_checkout.go_to_pricing_page(driver, base_url)
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.continue_as_currently_logged_in_user(driver)
    pages.express_checkout.select_the_first_saved_payment_method(driver)

    # WHEN
    pages.express_checkout.delete_the_only_vaulted_credit_card(driver)

    # THEN
    pages.express_checkout.i_should_not_see_any_vaulted_payment_methods(driver)


def test_express_checkout_new_user_new_credit_card_with_invalid_cvv(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, base_url)

    # WHEN
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method with invalid cvv
    pages.express_checkout.add_credit_card_payment_method(driver, cvv='201')
    pages.express_checkout.enter_new_user_invoice_details(driver)
    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the error
    assert pages.WebDriverWait(driver, 5).until(
        pages.EC.text_to_be_present_in_element((pages.By.XPATH, "//span[@class='error-msg']"),
                                               'CVV invalid. Please try again.'))


def test_express_checkout_user_prevented_from_purchasing_multiple_subscriptions(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, base_url)

    # WHEN
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method
    pages.express_checkout.add_credit_card_payment_method(driver, cvv=111)

    pages.express_checkout.enter_new_user_invoice_details(driver)

    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    pages.express_checkout.the_subscription_is_successful(driver, "Serato DJ Suite Subscription", email)

    # GIVEN
    # I add another subscription to my cart
    pages.express_checkout.go_to_pricing_page(driver, base_url)
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.continue_as_currently_logged_in_user(driver)
    pages.express_checkout.i_should_see_the_change_plan_message(driver)
    pages.express_checkout.i_can_view_the_subscriptions_faq(driver)


def test_express_checkout_new_user_new_paypal_payment(driver, globals):
    '''
    More about how the PayPal lightbox works in express checkout

    https://developer.paypal.com/docs/integration/direct/express-checkout/integration-jsv4/
    :return:
    '''
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, base_url)

    # WHEN
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method
    pages.express_checkout.add_paypal_payment_method(driver)

    pages.express_checkout.enter_new_user_invoice_details(driver)

    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    pages.express_checkout.the_purchase_is_successful(driver, "Serato DJ Suite", email)


def test_express_checkout_purchase_multiple_permanent_using_credit_card(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    base_url = globals['base_url']

    # GIVEN
    # I login
    pages.common_home.log_in_to_serato_dot_com(driver, base_url, email, password)

    # WHEN
    # I add 3 products to the cart
    driver.get('https://test-1-serato-com-us-east-1-aws.serato.net/checkout/cart/item/add/118,85,93')

    # THEN
    # I should see 3 products listed with the correct prices
    products = driver.find_elements_by_xpath("//*[@class='emphasis']")
    prices = driver.find_elements_by_xpath("//*[@class='amount emphasis']/div")
    assert len(products) == 3
    assert prices[0].text == '99.00' and prices[1].text == '29.00' and prices[2].text == '79.00'

    # WHEN
    # I continue the checkout process
    pages.express_checkout.continue_order_checkout(driver)

    # THEN
    # I see the Confirm User page with the email I just logged in with
    pages.express_checkout.i_should_see_my_user_on_the_confirm_user_page_as(driver, email)

    # WHEN
    # I click on the 'Continue' button
    pages.express_checkout.continue_to_payment_page(driver)

    # THEN
    # I should see the correct checkout details with 3 products in the order summary form
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)
    final_products = driver.find_elements_by_xpath("//*[@class='order-list']")
    final_prices = driver.find_elements_by_xpath("//*[@class='order-price']")
    assert len(final_products) == 3
    assert final_prices[0].text == '99.00' and final_prices[1].text == '29.00' and final_prices[2].text == '79.00'

    # WHEN
    # I click on the 'Pay Now' button
    pages.express_checkout.select_the_first_saved_payment_method(driver)
    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the correct success message with 3 products listed
    pages.express_checkout.the_purchase_is_successful(driver, "Multiple products", email, 3)


def test_express_checkout_add_multiple_subscriptions_of_same_id_and_purchase_single_subscription_using_credit_card(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']

    # WHEN
    # I add 2 subscriptions of the same product id to the cart
    driver.get('https://test-1-serato-com-us-east-1-aws.serato.net/checkout/cart/item/add/103,103')

    # THEN
    # I should see 1 subscription listed with the correct price
    products = driver.find_elements_by_xpath("//*[@class='emphasis']")
    prices = driver.find_elements_by_xpath("//*[@class='amount emphasis']/div")
    assert len(products) == 1
    assert prices[0].text == '9.99'

    # WHEN
    # I continue the checkout process and create a new account
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    # I should see the correct checkout details with 1 subscription in the order summary form
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)
    final_products = driver.find_elements_by_xpath("//*[@class='order-list']")
    final_prices = driver.find_elements_by_xpath("//*[@class='order-price']")
    assert len(final_products) == 1
    assert final_prices[0].text == '9.99'

    # WHEN
    # I add payment and invoice details and click on the 'Pay Now' button
    pages.express_checkout.add_credit_card_payment_method(driver, cvv=111)
    pages.express_checkout.enter_new_user_invoice_details(driver)
    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the correct success message with 1 subscription listed
    pages.express_checkout.the_subscription_is_successful(driver, "Serato DJ Pro Subscription", email)


def test_express_checkout_add_subscription_and_license_and_purchase_subscription_only_using_credit_card(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    email = pages.common_pop3.append_timestamp_to_email(email)
    password = globals['test_accounts'][0]['valid_password']

    # WHEN
    # I add 1 subscription and 1 license to the cart
    driver.get('https://test-1-serato-com-us-east-1-aws.serato.net/checkout/cart/item/add/105,73')

    # THEN
    # I should see 1 subscription listed with the correct price
    products = driver.find_elements_by_xpath("//*[@class='emphasis']")
    prices = driver.find_elements_by_xpath("//*[@class='amount emphasis']/div")
    assert len(products) == 1
    assert prices[0].text == '14.99'

    # WHEN
    # I continue the checkout process and create a new account
    pages.express_checkout.continue_order_checkout(driver)
    pages.express_checkout.create_account(driver, email, password)

    # THEN
    # I should see the correct checkout details with 1 subscription in the order summary form
    pages.express_checkout.i_should_see_the_payment_details_form(driver)
    pages.express_checkout.i_should_see_the_invoice_details_form(driver)
    pages.express_checkout.i_should_see_the_order_summary_form(driver)
    final_products = driver.find_elements_by_xpath("//*[@class='order-list']")
    final_prices = driver.find_elements_by_xpath("//*[@class='order-price']")
    assert len(final_products) == 1
    assert final_prices[0].text == '14.99'

    # WHEN
    # I add payment and invoice details and click on the 'Pay Now' button
    pages.express_checkout.add_credit_card_payment_method(driver, cvv=111)
    pages.express_checkout.enter_new_user_invoice_details(driver)
    pages.express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the correct success message with 1 subscription listed
    pages.express_checkout.the_subscription_is_successful(driver, "Serato DJ Suite Subscription", email)


def test_reset_password_using_express_checkout_mobile_error_scenarios(mobile_driver, globals):
    # # With "recent" mode enabled, Gmail will check the last 30 days' mail,
    # # even if it has already been downloaded elsewhere. MUST prepend the
    # # email address with "recent:" otherwise the messages won't show up
    # # in the POP connection if accessed prior with Gmail in the browser.
    # # https://www.lifewire.com/g00/get-gmail-in-all-programs-and-devices-with-recent-mode-1172096?i10c.referrer=https%3A%2F%2Fwww.google.co.nz%2F
    #
    # email = globals['test_accounts'][0]['valid_email']
    # recent_email = "recent:" + email
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(mobile_driver, base_url)
    pages.express_checkout.add_dj_suite_subscription_to_cart(mobile_driver)
    pages.express_checkout.continue_order_checkout(mobile_driver)

    # WHEN
    pages.express_checkout.reset_password_error_scenarios(mobile_driver)


def test_reset_password_using_express_checkout_mobile(mobile_driver, globals):
    # With "recent" mode enabled, Gmail will check the last 30 days' mail,
    # even if it has already been downloaded elsewhere. MUST prepend the
    # email address with "recent:" otherwise the messages won't show up
    # in the POP connection if accessed prior with Gmail in the browser.
    # https://www.lifewire.com/g00/get-gmail-in-all-programs-and-devices-with-recent-mode-1172096?i10c.referrer=https%3A%2F%2Fwww.google.co.nz%2F

    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    recent_email = "recent:" + email
    base_url = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(mobile_driver, base_url)
    pages.express_checkout.add_dj_suite_subscription_to_cart(mobile_driver)
    pages.express_checkout.continue_order_checkout(mobile_driver)

    # WHEN
    latest_password_reset_url = pages.express_checkout.reset_password_for_existing_user(mobile_driver, email, recent_email, password)

    # THEN
    # Go to password reset URL, then log in with new password
    pages.express_checkout.visit_the_password_reset_link_and_reset_password(mobile_driver, latest_password_reset_url, password)


def test_reset_password_using_express_checkout_desktop(driver, globals):
    # With "recent" mode enabled, Gmail will check the last 30 days' mail,
    # even if it has already been downloaded elsewhere. MUST prepend the
    # email address with "recent:" otherwise the messages won't show up
    # in the POP connection if accessed prior with Gmail in the browser.
    # https://www.lifewire.com/g00/get-gmail-in-all-programs-and-devices-with-recent-mode-1172096?i10c.referrer=https%3A%2F%2Fwww.google.co.nz%2F
    test_env = globals['base_url']
    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    recent_email = "recent:" + email

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # WHEN
    latest_password_reset_url = pages.express_checkout.reset_password_for_existing_user(driver, email, recent_email, password)

    # THEN
    # Go to password reset URL, then log in with new password
    pages.express_checkout.visit_the_password_reset_link_and_reset_password(driver, latest_password_reset_url, password)


def test_request_to_signup_with_new_email_is_successful():
    email = "test.user+" + pages.datetime.datetime.now().strftime("%B%d%Y%H%M%S") + "@serato.com"
    result = pages.checkoutlib_requesters.signup_requester(test_environment=1, email=email, password='111222')
    assert result['emailAddress'] == email.lower()


def test_request_to_signup_with_existing_email_is_unsuccessful():
    result = pages.checkoutlib_requesters.signup_requester(test_environment=1, email='test.user@serato.com', password='111222')
    assert result['error'] == 'A user account with the email address already exists'


def test_request_to_signup_with_weak_password_is_unsuccessful():
    email = "test.user+" + pages.datetime.datetime.now().strftime("%B%d%Y%H%M%S") + "@serato.com"
    result = pages.checkoutlib_requesters.signup_requester(test_environment=1, email=email, password='111')
    assert result['error'] == 'Uncompliant password'


def test_sign_up_with_new_email(driver, globals):
    timestamp = pages.misc.get_datetime_now_string()
    user_from_json = globals['test_accounts'][0]['valid_email']
    # Create a unique user email which has the current timestamp appended to it
    email = user_from_json.split("@")[0] + "+" + timestamp + "@" + user_from_json.split("@")[1]
    password = globals['test_accounts'][0]['valid_password']
    country = globals['country']
    test_env = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.log_in_page.create_account_button(driver).click()

    # WHEN
    # I sign up with new email
    pages.express_checkout.signup(driver, email, password, country)

    # THEN
    # I should see the Payment page
    pages.express_checkout.i_should_see_the_payment_details_form(driver)


def test_sign_up_with_existing_email(driver, globals):
    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    country = globals['country']
    test_env = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_purchase_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.log_in_page.create_account_button(driver).click()

    # WHEN
    # I sign up with existing user
    pages.express_checkout.signup(driver, email, password, country)

    # THEN
    # I get the error message
    assert pages.sign_up_page.error_message_text(
        driver).text == 'A user account with the email address already exists'


def test_sign_up_using_invalid_email(driver, globals):
    email = globals['test_accounts'][1]['invalid_email']
    password = globals['test_accounts'][1]['invalid_password']
    country = globals['country']
    test_env = globals['base_url']

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)
    pages.log_in_page.create_account_button(driver).click()

    # WHEN
    # I sign up with an invalid email
    pages.express_checkout.signup(driver, email, password, country)

    # THEN
    # I should see the invalid email error
    assert pages.sign_up_page.email_error_message_text(driver).text == 'The Email address field must be a valid email.'