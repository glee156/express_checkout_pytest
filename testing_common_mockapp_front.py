from tests.web.WEB.MySerato_mockapp.Pages import common_mockapp_front
from tests.web.WEB.MySerato_mockapp.Pages import common_subscriptions
import logging
from selenium.webdriver.remote.remote_connection import LOGGER
from tests.web.web_globalvariables import TESTUSER_EMAIL, TESTUSER_PWD
from tests.web.WEB.Seratodotcom.Steps import express_checkout
from tests.web.UTILITIES import read_data
from tests.web.UTILITIES.Email_functions import common_pop3
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import time
from tests.web.WEB.Seratodotcom.Pages import common_home


def test_complete_mockapp_payment_details_with_error_message(driver):
    LOGGER.setLevel(logging.WARNING)
    billing_country = 'NZ'
    test_environment = 7

    # GIVEN
    # I log in using the My Serato window
    # common_mockapp_front.go_to_mockapp_only_window(driver, test_environment)
    common_mockapp_front.go_to_mockapp(driver, test_environment)
    common_mockapp_front.open_mockapp_window(driver)
    # driver.find_element_by_xpath("//button[@class='button try-activate-clubkit']").click()
    common_mockapp_front.log_in_to_mockapp_with_unique_timestamped_email(driver, billing_country)
    # common_mockapp_front.log_in_to_mockapp_with_existing_email(driver, TESTUSER_EMAIL, TESTUSER_PWD)

    # WHEN
    # I buy expansion using braintree
    # common_mockapp_front.add_FX_pack_to_cart(driver)
    # assert driver.find_element_by_xpath("(//a[contains(@href, 'javascript')]/span)[2]").text == 'New Zealand'
    # logging.info("New Zealand found")

    # I fill in payment details on mockapp
    # common_mockapp_front.add_FX_pack_to_cart(driver)
    # common_mockapp_front.complete_mockapp_payment_details(driver, billing_country)

    # I add video rlm license to cart
    # common_mockapp_front.add_video_rlm_to_cart(driver)
    # logging.info("added video to cart")

    # I plug in hardware
    # common_subscriptions.plug_in_trial_hardware(driver)
    # WebDriverWait(driver, 20).until(EC.invisibility_of_element_located((By.XPATH, "//*[contains(@data-original-english, 'Compatible hardware is required')]")))

    # I buy dj pro subscription and change to dj suite
    # common_subscriptions.click_serato_dj_subscription(driver)
    # common_mockapp_front.select_the_hardware_i_am_buying_for(driver)
    # common_mockapp_front.complete_mockapp_payment_details(driver, billing_country, "Subscribe")
    # common_subscriptions.go_to_manage_subscription(driver)
    # common_subscriptions.change_subscription_to_dj_suite(driver)


def test_deactivate_subscription(driver):
    LOGGER.setLevel(logging.WARNING)
    billing_country = 'NZ'
    test_environment = 7
    email = read_data.return_value_from_key("NZ", 'email', 'Test_User_Tax_Rates.json')
    email = common_pop3.append_timestamp_to_email(email)
    password = read_data.return_value_from_key("NZ", 'password', 'Test_User_Tax_Rates.json')

    # GIVEN
    # I add SDJ to my product cart using express checkout
    express_checkout.go_to_pricing_page(driver, test_environment)

    # WHEN
    express_checkout.add_dj_suite_subscription_to_cart(driver)
    express_checkout.continue_order_checkout(driver)
    express_checkout.create_account(driver, email, password)

    # THEN
    express_checkout.i_should_see_the_payment_details_form(driver)
    express_checkout.i_should_see_the_invoice_details_form(driver)
    express_checkout.i_should_see_the_order_summary_form(driver)

    # WHEN
    # I add a payment method
    express_checkout.add_credit_card_payment_method(driver, cvv=111)

    express_checkout.enter_new_user_invoice_details(driver)

    express_checkout.i_click_pay_now_on_the_payment_page(driver)

    # THEN
    # I should see the payment success confirmation page
    # express_checkout.the_purchase_is_successful(driver, "Serato DJ Pro", email)
    express_checkout.the_subscription_is_successful(driver, "Serato DJ Suite Subscription", email)

    # WHEN
    # I deactivate on mockapp
    # common_mockapp_front.go_to_mockapp(driver, test_environment)
    # common_mockapp_front.open_mockapp_window(driver)
    # common_subscriptions.deactivate_subscription(driver)

    # WHEN
    # I update billing details
    common_mockapp_front.go_to_mockapp(driver, test_environment)
    common_mockapp_front.open_mockapp_window(driver)
    common_mockapp_front.navigate_to_mockapp_tab(driver, "My Products")
    common_subscriptions.go_to_manage_subscription(driver)
    common_subscriptions.update_billing_details(driver)