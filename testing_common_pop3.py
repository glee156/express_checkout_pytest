from utilities import poll_t
import time
from utilities import log
from selenium.webdriver.remote.remote_connection import LOGGER
import logging
from tests.web.UTILITIES.Email_functions import common_pop3 as p
from tests.web.UTILITIES.Email_steps import check_email as email_steps
import pages as pages
from tests.web.WEB.Seratodotcom.Steps import signup_and_login
from tests.web.WEB.Mailchimp.Steps import verification
from utilities import misc


'''uses get_latest_messages and parse_for_keyword_and_delete functions of common_pop3'''
def test_reset_password_using_express_checkout_desktop(driver, globals):
    # With "recent" mode enabled, Gmail will check the last 30 days' mail,
    # even if it has already been downloaded elsewhere. MUST prepend the
    # email address with "recent:" otherwise the messages won't show up
    # in the POP connection if accessed prior with Gmail in the browser.
    # https://www.lifewire.com/g00/get-gmail-in-all-programs-and-devices-with-recent-mode-1172096?i10c.referrer=https%3A%2F%2Fwww.google.co.nz%2F
    test_env = globals['base_url']
    email = globals['test_accounts'][0]['valid_email']
    password = globals['test_accounts'][0]['valid_password']
    recent_email = "recent:" + email

    # GIVEN
    # I add SDJ to my product cart using express checkout
    pages.express_checkout.go_to_pricing_page(driver, test_env)
    pages.express_checkout.add_dj_suite_subscription_to_cart(driver)
    pages.express_checkout.continue_order_checkout(driver)

    # WHEN
    latest_password_reset_url = pages.express_checkout.reset_password_for_existing_user(driver, email, recent_email, password)

    # THEN
    # Go to password reset URL, then log in with new password
    pages.express_checkout.visit_the_password_reset_link_and_reset_password(driver, latest_password_reset_url, password)


'''uses append_timestamp_to_email function of common_pop3'''
def test_signup_in_my_serato(driver):

    test_environment = 1
    email = p.append_timestamp_to_email("test.user+mailchimp_com@serato.com")

    # GIVEN
    # I sign up to serato.com
    pages.common_home.go_to_configurable_test_serato_dot_com(driver, test_environment)
    signup_and_login.login_to_serato_com_from_homepage(driver, email)


'''uses get_all_messages function of common_pop3. Takes like 3 minutes though'''
def test_verify_email_address():
    import poplib
    # Need to redefine to avoid maxline error when parsing email inbox - bug with python 2.7.6
    poplib._MAXLINE = 20480
    pop_conn = poplib.POP3_SSL('pop.gmail.com')
    # Login to mail server
    pop_conn.user('recent: test.user@serato.com')
    pop_conn.pass_('Auckland009')

    print "Getting messages " + str(misc.get_datetime_now_string())
    messages = p.get_all_messages(pop_conn)
    print "Got messages " + str(misc.get_datetime_now_string())
    # Look for the keyword "Join"
    p.parse_for_keyword_and_delete(messages, pop_conn, "Join")


# def test_open_page(driver, globals):
#     # Given I'm on the home page of test stack 6
#     driver.get("https://test-8-serato-com-us-east-1-aws.serato.net/")
#
#     # When I click on the link to the Scratch Live page
#     driver.find_element_by_xpath("//a[contains(text(),'Products')]").click()
#     driver.find_element_by_xpath("//a[contains(text(),'Scratch Live')]").click()
#
#     # Then I should see the new Scratch live page
#     time.sleep(3)
#     #log.info_separator("On new Scratch Live download page")
#
#     # When I click on the download link
#     driver.find_element_by_xpath("//a[contains(@href,'/scratchlive/downloads')]").click()
#
#     # Then I should see the new Scratch Live download page
#     time.sleep(3)
#
#     # And I should be able to click on all the accordions
#     accordions = driver.find_elements_by_xpath("//div[@class='card no-border accordion-wrap']")
#
#     for accordion in accordions:
#         accordion.click()
#
#     # And I should be able to download Scratch Live
#     #driver.find_element_by_xpath("//a[contains(@href,'/scratchlive/downloads/mac')]").click()
#
#
# def test_email_functions(driver, globals):
#     import poplib
#     username = globals['test_accounts'][0]['valid_email']
#     password = globals['test_accounts'][0]['valid_password']
#     poplib._MAXLINE = 20480
#
#     pop_conn = poplib.POP3_SSL('pop.gmail.com')
#     pop_conn.user(username)
#     pop_conn.pass_(password)
#
#     messages = email_steps.subject_parse_and_delete("date")